from django.db import models

# Create your models here.
class Jadwal(models.Model):
    mata_kuliah = models.CharField(blank=False, max_length= 100)
    dosen = models.CharField(blank=False, max_length= 100)
    jumlah_sks = models.CharField(blank=False, max_length= 100)
    deskripsi = models.CharField(blank=False, max_length= 100)
    semester = models.CharField(blank=False, max_length=100)
    ruang_kelas = models.CharField(blank=False, max_length= 100)