from django import forms

class formulir(forms.Form):
    mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Mata Kuliah',
        'type' : 'text',
        'required' : True
    }))
    dosen = forms.CharField(widget=forms.DateTimeInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Dosen',
        'type' : 'text',
        'required' : True
    }))
    jumlah_sks = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah sks',
        'type' : 'text',
        'required' : True
    }))
    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsi',
        'type' : 'text',
        'required' : True
    }))
    semester = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Semester',
        'type' : 'text',
        'required' : True
    }))
    ruang_kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Ruang Kelas',
        'type' : 'text',
        'required' : True
    }))
