from django.urls import path
from . import views


app_name = 'jadwal'

urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    path('<int:pk>/', views.delete, name = 'hapus'),
    # dilanjutkan ...
]