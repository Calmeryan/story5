from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def jadwal(request):
    if(request.method == "POST"):
        tmp = forms.formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.Jadwal()
            tmp2.mata_kuliah = tmp.cleaned_data["mata_kuliah"]
            tmp2.dosen = tmp.cleaned_data["dosen"]
            tmp2.jumlah_sks = tmp.cleaned_data["jumlah_sks"]
            tmp2.deskripsi = tmp.cleaned_data["deskripsi"]
            tmp2.semester = tmp.cleaned_data["semester"]
            tmp2.ruang_kelas = tmp.cleaned_data["semester"]
            tmp2.save()
        return redirect("/")
    else:
        tmp = forms.formulir()
        tmp2 = models.Jadwal.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'jadwal' : tmp2
        }
        return render(request, 'jadwal.html', tmp_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        tmp = forms.formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.Jadwal()
            tmp2.mata_kuliah = tmp.cleaned_data["mata_kuliah"]
            tmp2.dosen = tmp.cleaned_data["dosen"]
            tmp2.jumlah_sks = tmp.cleaned_data["jumlah_sks"]
            tmp2.deskripsi = tmp.cleaned_data["deskripsi"]
            tmp2.semester = tmp.cleaned_data["semester"]
            tmp2.ruang_kelas = tmp.cleaned_data["semester"]
            tmp2.save()
        return redirect("/")
    else:
        models.Jadwal.objects.filter(pk = pk).delete()
        tmp = forms.formulir()
        tmp2 = models.Jadwal.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'jadwal' : tmp2  
        }
        return render(request, 'jadwal.html', tmp_dictio)